angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
  .state('dBConsultorias', {
    url: '/login',
    templateUrl: 'templates/dBConsultorias.html',
    controller: 'dBConsultoriasCtrl'
  })

  .state('agendamentosConsultor', {
    url: '/',
    templateUrl: 'templates/agendamentosConsultor.html',
    controller: 'agendamentosConsultorCtrl'
  })

  .state('agendamentosAdmin', {
    url: '/detalhesConsultoria',
    templateUrl: 'templates/agendamentosAdmin.html',
    controller: 'agendamentosAdminCtrl'
  })

  .state('agendamentosSolicitante', {
    url: '/',
    templateUrl: 'templates/agendamentosSolicitante.html',
    controller: 'agendamentosSolicitanteCtrl'
  })

  .state('infoSolicitante', {
    url: '/infoSolicitante',
    templateUrl: 'templates/infoSolicitante.html',
    controller: 'infoSolicitanteCtrl'
  })

  .state('infoConsultor', {
    url: '/infoConsultor',
    templateUrl: 'templates/infoConsultor.html',
    controller: 'infoConsultorCtrl'
  })

  .state('infoSchedule', {
      url: '/schedule/:scheduleId',
      templateUrl: 'templates/infoSchedule.html',
      controller: 'infoScheduleCtrl'
  })

  .state('infoAdmin', {
    url: '/infoAdmin',
    templateUrl: 'templates/infoAdmin.html',
    controller: 'informaEsAgendamentoCtrl'
  })

$urlRouterProvider.otherwise('/login')



});
