var requisitions = {
  "Requisitions": [{
    id: 4,
    initDate: "2016-08-13 10:00:00.0",
    endDate: "2016-08-13 14:00:00.0",
    address: "Av. Ipiranga, 6681",
    consultant: "Tiago Totti",
    client: "Procergs",
    description: "Necessitamos consultoria para um novo projeto de aplicações móveis. Não temos preferência por consultor.",
    user: "Gustavo Gust",
    service: "Consultorias",
    subject: "Mobilidade",
    status: "Pendente",
    reason: "null"
  }, {
    id: 1,
    initDate: "2016-08-22 08:00:00.0",
    endDate: "2016-08-02 18:00:00.0",
    address: "Av. Ipiranga, 1200",
    consultant: "Jorge Audy",
    client: "DBServer ",
    description: "null",
    user: "Eduardo Peres",
    service: "Consultorias",
    subject: "Métodos Ágeis",
    status: "Aprovado",
    reason: "null"
  }, {
    id: 2,
    initDate: "2016-08-23 09:30:00.0",
    endDate: "2016-08-23 17:30:00.0",
    address: "Av. Ipiranga, 1200",
    consultant: "Marina Bellemzier",
    client: "Procempa",
    description: "Necessitamos de uma consultoria de metodologias ágeis para o projeto de Natal. Gostaríamos de ser atendidos pelo Jorge Audy.",
    user: "Carlos Gomide",
    service: "Consultorias",
    subject: "Métodos Ágeis",
    status: "Aprovado",
    reason: "null"
  }, {
    id: 7,
    initDate: "2016-08-24 09:30:00.0",
    endDate: "2016-08-24 17:30:00.0",
    address: "Av. Ipiranga, 1200",
    consultant: "Jorge Audy",
    client: "Procempa",
    description: "Necessitamos de uma consultoria pelo Jorge Audy.",
    user: "Bruno Braga",
    service: "Consultorias",
    subject: "Inception",
    status: "Aprovado",
    reason: "null"
  }, {
    id: 3,
    initDate: "2016-08-25 09:30:00.0",
    endDate: "2016-08-25 17:30:00.0",
    address: "Av. Assis Brasil, 3940 - Sarandi",
    consultant: "Jorge Audy",
    client: "Sicredi",
    description: "Gostaríamos de ser atendidos pelo Jorge Audy.",
    user: "Cláudio Pacheco",
    service: "Consultorias",
    subject: "Métodos Ágeis",
    status: "Pendente",
    reason: "null"
  }, {
    id: 6,
    initDate: "2016-09-09 09:30:00.0",
    endDate: "2016-09-09 14:00:00.0",
    address: "Av. Ipiranga, 1200",
    consultant: "Rafael Piter",
    client: "Paquetá",
    description: "Precisamos de um coaching abordando os processos e ferramentas envolvidas em Devops. Não temos preferência por consultor.",
    user: "Joao Firmino",
    service: "Coaching",
    subject: "Devops",
    status: "Pendente",
    reason: "null"
  }, {
    id: 9,
    initDate: "2016-09-14 08:30:00.0",
    endDate: "2016-09-14 17:15:00.0",
    address: "Av. Teresópolis",
    consultant: "Eduardo Peres",
    client: "Vonpar",
    description: "Gostaríamos de solicitar um treinamento sobre arquitetura .NET. Gostaríamos de ser atendidos pelo Eduardo Peres.",
    user: "Ricardo Vontobel",
    service: "Consultorias",
    subject: "PMO Ágil",
    status: "Cancelado",
    reason: "null"
  }, {
    id: 8,
    initDate: "2016-09-14 10:00:00.0",
    endDate: "2016-09-14 15:30:00.0",
    address: "Praça dos Açorianos S/Nº",
    consultant: "Carolina Veeck",
    client: "Procempa",
    description: "Necessitamos de uma consultoria de ponto de função. Gostaríamos de ser atendidos pela Carolina.",
    user: "Carlos Gomide",
    service: "Consultorias",
    subject: "Pontos de Função",
    status: "Cancelado",
    reason: "null"
  }, {
    id: 10,
    initDate: "2016-09-14 10:00:00.0",
    endDate: "2016-09-14 15:30:00.0",
    address: "Praça dos Açorianos S/Nº",
    consultant: "Jorge Audy",
    client: "Procempa",
    description: "Necessitamos de um treinamento sobre Inception. Gostaríamos de ser atendidos pela Audy.",
    user: "Bruno Braga",
    service: "Treinamentos",
    subject: "Inception",
    status: "Cancelado",
    reason: "null"
  }, {
    id: 5,
    initDate: "2016-09-15 13:00:00.0",
    endDate: "2016-09-15 17:00:00.0",
    address: "Av. Ipiranga, 1200",
    consultant: "Leonardo Duprates",
    client: "Procempa",
    description: "Treinamento com TDD para os desenvolvedores da equipe. Gostaríamos de ser atendidos pelo Tiago.",
    user: "Bruno Braga",
    service: "Coaching",
    subject: "Automação de Testes",
    status: "Pendente",
    reason: "null"
  }]
};
angular.module('app.controllers', [])

.controller('dBConsultoriasCtrl', ['$scope', '$stateParams', '$ionicPopup', '$state', 'LoginService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
  // You can include any angular dependencies as parameters for this function
  // TIP: Access Route Parameters for your page via $stateParams.parameterName
  function($scope, $stateParams, $ionicPopup, $state, LoginService) {
    $scope.username = "";
    $scope.password = "";
    $scope.login = function() {
      //alert('Username: ' + $scope.username + ' Senha: ' + $scope.password);
      LoginService.loginUser($scope.username, $scope.password).success(function(data) {
          $state.go(data);
      }).error(function(data) {
          var alertPopup = $ionicPopup.alert({
              title: 'login inválido!',
              template: 'Favor verifique se as informações digitadas estão corretas!'
          });
      });
    };
  }
])

.controller('agendamentosConsultorCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
  // You can include any angular dependencies as parameters for this function
  // TIP: Access Route Parameters for your page via $stateParams.parameterName
  function($scope, $stateParams) {
    $scope.requisitions = requisitions.Requisitions.filter(isConsultant);
    function isConsultant(req) {
      return req.consultant == 'Jorge Audy';
    }
  }
])

.controller('agendamentosAdminCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
  // You can include any angular dependencies as parameters for this function
  // TIP: Access Route Parameters for your page via $stateParams.parameterName
  function($scope, $stateParams) {
    $scope.requisitions = requisitions.Requisitions;


  }
])

.controller('agendamentosSolicitanteCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
  // You can include any angular dependencies as parameters for this function
  // TIP: Access Route Parameters for your page via $stateParams.parameterName
  function($scope, $stateParams) {
    $scope.requisitions = requisitions.Requisitions.filter(isRequester);
    function isRequester(req) {
      return req.user == 'Bruno Braga';
    }
  }
])

.controller('informaEsAgendamentoCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
  // You can include any angular dependencies as parameters for this function
  // TIP: Access Route Parameters for your page via $stateParams.parameterName
  function($scope, $stateParams) {


  }
])

.controller('infoScheduleCtrl', ['$scope', '$stateParams',
  function($scope, $stateParams) {
    $scope.reqID = $stateParams.scheduleId;
    for (i = 0; i < requisitions.Requisitions.length; i++) {
      if (requisitions.Requisitions[i].id == $scope.reqID) {
        $scope.req = requisitions.Requisitions[i];
        break;
      }
    }
  }
])
